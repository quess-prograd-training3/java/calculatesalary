import java.util.Scanner;

public class BonusAndSalaryCalculation {
        private int mon;
        private int tue;
        private int wed;
        private int thu;
        private int fri;
        private int sat;
        private int sun;

        public void setNumber() {
            Scanner scannerObject = new Scanner(System.in);
            System.out.println("Enter the hours for each day from monday to sunday");
            sun = scannerObject.nextInt();
            mon = scannerObject.nextInt();
            tue = scannerObject.nextInt();
            wed = scannerObject.nextInt();
            thu = scannerObject.nextInt();
            fri = scannerObject.nextInt();
            sat = scannerObject.nextInt();

        }

        int dailyBounce, sal1 = 0, sal2 = 0, sal3 = 0, sal4 = 0, sal5 = 0, sal6 = 0, sal7 = 0;

        public void everyDayIncome() {
            if (mon <= 8) {
                sal1 = mon * 100;
            } else if (mon > 8) {
                dailyBounce = (mon - 8) * 115;
                sal1 = dailyBounce + 800;
            }
            if (tue <= 8) {
                sal2 = tue * 100;
            } else if (tue > 8) {
                dailyBounce = (tue - 8) * 115;
                sal2 = dailyBounce + 800;
            }
            if (wed <= 8) {
                sal3 = wed * 100;
            } else if (wed > 8) {
                dailyBounce = (wed - 8) * 115;
                sal3 = dailyBounce + 800;
            }
            if (thu <= 8) {
                sal4 = thu * 100;
            } else if (thu > 8) {
                dailyBounce = (thu - 8) * 115;
                sal4 = dailyBounce + 800;
            }
            if (fri <= 8) {
                sal5 = fri * 100;
            } else if (fri > 8) {
                dailyBounce = (fri - 8) * 115;
                sal5 = dailyBounce + 800;
            }
            int totalDays = mon + tue + wed + thu + fri;
            int extraDays = 0;
            int extraRate = 0;
            if (totalDays > 40) {
                extraDays = totalDays - 40;
                extraRate = extraDays * 25;
            }
            sal6 = sat * 125;
            sal7 = sun * 150;
            int totalSalary = sal1 + sal2 + sal3 + sal4 + sal5 + sal6 + sal7 + extraRate;
            System.out.println(totalSalary);

        }

}
